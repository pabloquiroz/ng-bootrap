import { ToastContainerService } from './../../projects/bootstrap/src/lib/services/toast-container.service';
import { BootstrapService } from './../../projects/bootstrap/src/lib/bootstrap.service';
import { User, Post } from './models';
import { Observable, combineLatest, of, forkJoin, from, BehaviorSubject } from 'rxjs';
import { JsonPlaceHolderService } from './services/json-place-holder.service';
import { Component, OnInit } from '@angular/core';
import { tap, map, flatMap, filter, mergeMap, toArray, mergeAll, delay } from 'rxjs/operators';

@Component({
  selector: 'app-root',
  template: `
    <!--The content below is only a placeholder and can be replaced.-->
    <bt-header></bt-header>
    <div *ngIf="loadingObs$ | loading | async as obsResult">
      <div *ngIf="obsResult.loading">
        <div class="d-flex justify-content-center">
          <div class="spinner-border text-danger" role="status">
            <span class="sr-only">Loading...</span>
          </div>
        </div>
      </div>
      <div *ngIf="obsResult.value">
        <main class="bd-masthead">
          <div class="container">
            <div class="row">
              <bt-jumbotron [componentTemplate]="componentChart"></bt-jumbotron>
              <ng-template #componentChart>
                <prime-chart-area [dataChart]="dataChart"></prime-chart-area>
              </ng-template>
            </div>
          </div>
        </main>
        <div class="container-fluid p-3 p-md-5">
          <div class="col-md-12 p-3 p-md-5 bg-light border border-white">
            <div class="row">
              <button type="button" class="btn btn-primary" (click)="createModal(content)">Create User</button>
            </div>
            <div class="row-table">
              <bt-table [data]="userPost" (updateEvent$)="updateModal($event, content)" (deleteEvent$)="delete($event)"></bt-table>
            </div>
          </div>
        </div>
      </div>
    </div>

    <ng-template #content let-modal>
      <div class="modal-header">
        <h4 class="modal-title" id="modal-basic-title">Update Posts</h4>
        <button type="button" class="close" aria-label="Close" (click)="modal.dismiss('Cross click')">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <app-form
        (fromUpdateEmitter)="updateData($event)"
        (formCreateEmitter)="createData($event)"
        [data]="dataForm"
        ></app-form>
      </div>
      <!-- <div class="modal-footer">
        <button type="button" class="btn btn-outline-dark" (click)="modal.close('Save click')">Save</button>
      </div> -->
    </ng-template>
  `,
  styles: []
})
export class AppComponent implements OnInit {
  title = 'Bootstrap';
  users: Observable<User[]>;
  posts: Observable<any[]>;
  postByUser: Observable<any[]>;
  userPost: Observable<any[]>;
  dataChart: Observable<any>;

  loadingObs$ = of(1).pipe(delay(1000));

  // Form Prop
  dataForm: Observable<any>;

  //

  constructor(
    private jsonPlaceHolderService: JsonPlaceHolderService,
    private bootstrapService: BootstrapService,
    private toastService: ToastContainerService
  ) {}

  ngOnInit() {
    this.getUsers();
    this.getPosts();
    this.count();
    this.mergeUserPost();
    this.fillChart();
  }

  // Get all users
  getUsers() {
    this.users = this.jsonPlaceHolderService.getUsers();
  }

  // Get all posts
  getPosts() {
    this.posts = this.jsonPlaceHolderService.getPosts();
  }

  count() {
    this.posts.pipe(
      map(post => { return post.map((data: Post) => {
          return data.userId;
        });
      }),
      tap(x => this.groupByPostId(x)),
    ).subscribe();
  }

  groupByPostId(data) {
    // Count all id by user
    const object = data.reduce((number, data) => { number[data] = (number[data] || 0) + 1; return number}, {});
    // Split in json object
    const splitObject = of(Object.keys(object).map(k => ({ [k]: object[k] })));
    // Asign keys id & count
    this.postByUser = splitObject.pipe(
      map(data => {
        return data.map(d => {
          const userId = +Object.keys(d)[0];
          const count = Object.values(d)[0];
          return {id: userId, count: count};
        });
      })
    );
  }

  mergeUserPost() {
    // Merge User with Post
    this.userPost = from(this.users).pipe(
      flatMap(fm => fm),
      mergeMap(data => {
        // Get Data from Post by Id
        const post$ = from(this.postByUser).pipe(
          flatMap(fm => fm),
          filter(post => post.id === data.id)
        );

        // Join data between user and post
        // tslint:disable-next-line: deprecation
        return forkJoin([post$], (post) => {
          return {
            id: data.id,
            name: data.name,
            username: data.username,
            email: data.email,
            idPost: post.id,
            count: post.count,
          };
        });
      }),
      toArray(),
    );
  }

  // Data Chart

  fillChart() {
    this.dataChart = this.userPost.pipe(
      map(chart => {
        let label = 'My dataset';
        let labels = [];
        let data = [];
        let backgroundColor = [
          '#FF6384',
          '#4BC0C0',
          '#FFCE56',
          '#E7E9ED',
          '#36A2EB',
          '#FF6384',
          '#4BC0C0',
          '#FFCE56',
          '#E7E9ED',
          '#36A2EB'
        ];

        chart.map(chartMap => {
          labels.push(chartMap.name);
          data.push(chartMap.count);
        });

        return {
          datasets: [
            {data: data, backgroundColor: backgroundColor, label: label}
          ],
          labels,
        };
      }),
    );
  }

  // Modal Form

  createModal(content) {
    this.dataForm = of([]);
    this.bootstrapService.open(content);
  }

  updateModal($event, content) {
    this.dataForm = of($event);
    this.bootstrapService.open(content);
  }

  // Create data
  createData($event) {
    this.bootstrapService.dimissAll();
    this.jsonPlaceHolderService.createUser($event).subscribe(x => console.log(x));
    this.toastService.showSuccess();
  }

  // Update data
  updateData($event) {
    this.bootstrapService.dimissAll();
    this.jsonPlaceHolderService.updateUser($event).subscribe(x => console.log(x));
  }

  // Delete Data
  delete($event) {
    const id = $event.id;
    this.jsonPlaceHolderService.deleteUser($event).subscribe(x => console.log(x));

    this.userPost = this.userPost.pipe(
      map(post => {
        return post.filter(result => result.id !== id);
      })
    );
  }
}



