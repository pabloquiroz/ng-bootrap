import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {NgbModule, NgbNavModule} from '@ng-bootstrap/ng-bootstrap';
import { AppRoutingModule } from './app-routing.module';
import { BootstrapModule } from 'projects/bootstrap/src/public-api';
import { PrimeFacesModule } from 'projects/prime-faces/src/public-api';
import { AngularFireModule } from '@angular/fire';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { AppComponent } from './app.component';
import { environment } from 'src/environments/environment';
import { LoadingPipe } from './pipes/loading.pipe';
import { FormComponent } from './components/form.component';

@NgModule({
  declarations: [
    AppComponent,
    LoadingPipe,
    FormComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BootstrapModule,
    PrimeFacesModule,
    NgbModule,
    NgbNavModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
    AngularFireModule.initializeApp(environment.firebase)
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
