import { Observable, of } from 'rxjs';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Component, EventEmitter, Output, OnInit, Input, OnChanges } from '@angular/core';
import { map, tap } from 'rxjs/operators';

@Component({
  selector: 'app-form',
  template: `
    <form *ngIf="fillForm | async" [formGroup]="form" (ngSubmit)="onSubmit()">
      <div class="form-group">
        <label for="inputName">Name </label>
        <input id="name" class="form-control" placeholder="Name" name="name" formControlName="name" #name required>
        <div class="invalid-feedback" *ngIf="isFieldInvalid('name')">
          <div *ngIf="!!control.name?.errors.required">Por favor, ingresa un nombre</div>
        </div>
      </div>
      <div class="form-group">
        <label for="InputEmail">Email address</label>
        <input id="email" class="form-control" placeholder="Email" name="email" formControlName="email" #email required>
        <div class="invalid-feedback" *ngIf="isFieldInvalid('email')">
          <div *ngIf="!!control.ubication?.errors.required">Por favor, ingresa una ubicación</div>
        </div> 
      </div>

      <div class="modal-footer">
        <button type="button" class="btn btn-outline-dark" *ngIf="(counter | async) === 0"
        [disabled]="!form.valid" type="submit">
          Crear
        </button>
        <button type="button" class="btn btn-outline-dark" *ngIf="(counter | async) !== 0"
        [disabled]="!form.valid" type="submit">
          Actualizar
        </button>
      </div>

    <!--
      <p>
        From counter: {{ counter | async }}
        Form Status: {{ form.status }}
      </p>
      </form>
    -->
  `,
  styles: [`

  `]
})
export class FormComponent implements OnInit, OnChanges {
  form: FormGroup;
  submitted = false;

  private formSubmitAttempt: boolean;
  fillForm: Observable<any>;
  @Input() data: Observable<any>;
  counter: Observable<number>;
  @Output() formCreateEmitter: EventEmitter<any> = new EventEmitter();
  @Output() fromUpdateEmitter: EventEmitter<any> = new EventEmitter();


  constructor(private fb: FormBuilder) {}

  ngOnInit() {
    this.counter = of(0);
    this.data.subscribe(x => this.counter = of(Object.keys(x).length));
    this.createForm();
  }

  ngOnChanges() {
    this.fillForm = this.data.pipe(
      tap(result => this.form.patchValue(result))
    );
    this.data.subscribe(x => this.counter = of(Object.keys(x).length));
  }

  createForm() {
    this.form = this.fb.group({
      name: ['', Validators.required],
      email: ['', Validators.required],
      id: [''],
    });
  }

  get control() {
    return this.form.controls;
  }

  isFieldInvalid(field: string) {
    return (
      (!this.form.get(field).valid && this.form.get(field).touched) ||
      (this.form.get(field).untouched && this.formSubmitAttempt)
    );
  }

  onSubmit() {
    this.submitted = true;
    const values = this.form.value;

    this.counter.subscribe(count => {
      if (count === 0) {
        // console.log('create FORM', values);
        this.formCreateEmitter.emit(values);
      } else if (count !== 0) {
        // console.log('update FORM', values);
        this.fromUpdateEmitter.emit(values);
      }
    });
    if (this.form.invalid) {
        return;
    }
  }

}
