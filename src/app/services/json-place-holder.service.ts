import { ToastContainerService } from './../../../projects/bootstrap/src/lib/services/toast-container.service';
import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http';
import { User, Post } from './../models';
import { forkJoin, Observable, throwError, of } from 'rxjs';
import { catchError } from 'rxjs/operators';

import { ToastService } from './../../../projects/bootstrap/src/lib/services/toast.service';

const users = 'https://jsonplaceholder.typicode.com/users';
const posts = 'https://jsonplaceholder.typicode.com/posts';
const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type':  'application/json',
  })
};

@Injectable({
  providedIn: 'root'
})
export class JsonPlaceHolderService {
  result: Observable<any>;

  private handleError(error: HttpErrorResponse) {
    if (error.error instanceof ErrorEvent) {
      // A client-side or network error occurred. Handle it accordingly.
      console.error('An error occurred:', error.error.message);
    } else {
      // The backend returned an unsuccessful response code.
      // The response body may contain clues as to what went wrong,
      console.error(
        `Backend returned code ${error.status}, ` +
        `body was: ${error.error}`);
    }
    // return an observable with a user-facing error message
    return throwError(
      'Something bad happened; please try again later.');
  }

  constructor(
    private http: HttpClient,
    public toastService: ToastService,
    public toastContainer: ToastContainerService) { }

  getUsers() {
    return this.http.get<User[]>(users);
  }

  getPosts() {
    return this.http.get<Post[]>(posts);
  }

  createUser(data: User) {
    return this.http.post(users, data, httpOptions);
  }

  updateUser(data: User) {
    return this.http.patch(`${users}/${data.id}`, data, httpOptions);
  }

  deleteUser(data: User) {
    return this.http.delete(`${users}/${data.id}`);
  }
}