import { Component } from '@angular/core';

@Component({
  selector: 'bt-nav',
  template: `
    <ul ngbNav #nav="ngbNav" class="navbar-nav bd-navbar-nav flex-row">
      <li ngbNavItem>
        <a ngbNavLink>First</a>
      </li>
      <li ngbNavItem>
        <a ngbNavLink>Second</a>
      </li>
    </ul>
  `,
  styles: []
})
export class NavComponent  {


}
