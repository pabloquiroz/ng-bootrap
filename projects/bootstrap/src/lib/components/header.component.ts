import { Component  } from '@angular/core';

@Component({
  selector: 'bt-header',
  template: `
    <header class="navbar navbar-expand navbar-dark flex-column flex-md-row bd-navbar">
      <a class="navbar-brand mr-0 mr-md-2" href="/" aria-label="Bootstrap">
        <bt-branding></bt-branding>
      </a>
      <div class="navbar-nav-scroll">
        <bt-nav></bt-nav>
      </div>
    </header>
  `,
  styles: []
})
export class HeaderComponent   {

 

}
