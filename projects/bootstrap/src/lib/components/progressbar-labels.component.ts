import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'bt-progressbar-labels',
  template: `
    <p><ngb-progressbar type="success" [value]="25">25</ngb-progressbar></p>
    <p><ngb-progressbar type="info" [value]="50">Copying file <b>2 of 4</b>...</ngb-progressbar></p>
    <p><ngb-progressbar type="warning" [value]="75" [striped]="true" [animated]="true"><i>50%</i></ngb-progressbar></p>
    <p><ngb-progressbar type="danger" [value]="100" [striped]="true">Completed!</ngb-progressbar></p>
  `,
  styles: [`
      ngb-progressbar {
        margin-top: 5rem;
      }
  `]
})
export class ProgressbarLabelsComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

}
