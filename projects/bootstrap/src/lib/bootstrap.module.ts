import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { NgbProgressbarModule, NgbNavModule } from '@ng-bootstrap/ng-bootstrap';

import { NavComponent } from './components/nav.component';
import { ProgressbarLabelsComponent } from './components/progressbar-labels.component';
import { BrandingComponent } from './components/branding.component';
import { HeaderComponent } from './components/header.component';
import { JumbotronComponent } from './components/jumbotron.component';
import { TableComponent } from './components/table.component';


@NgModule({
  declarations: [
    NavComponent,
    ProgressbarLabelsComponent,
    BrandingComponent,
    HeaderComponent,
    JumbotronComponent,
    TableComponent],
  imports: [
    CommonModule,
    NgbProgressbarModule,
    NgbNavModule,
  ],
  exports: [
    NavComponent,
    ProgressbarLabelsComponent,
    BrandingComponent,
    HeaderComponent,
    JumbotronComponent,
    TableComponent]
})
export class BootstrapModule { }
