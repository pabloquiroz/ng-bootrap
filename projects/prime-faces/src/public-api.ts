/*
 * Public API Surface of prime-faces
 */

export * from './lib/prime-faces.service';
export * from './lib/components/chart/polar-area-chart.component';
export * from './lib/prime-faces.module';
