import { TestBed } from '@angular/core/testing';

import { PrimeFacesService } from './prime-faces.service';

describe('PrimeFacesService', () => {
  let service: PrimeFacesService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(PrimeFacesService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
