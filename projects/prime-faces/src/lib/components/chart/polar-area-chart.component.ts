import { Observable, from, forkJoin } from 'rxjs';
import { Component, OnInit, Input } from '@angular/core';
import { tap, map, flatMap, filter, mergeAll, mergeMap } from 'rxjs/operators';

@Component({
  selector: 'prime-chart-area',
  template: `
    <p-chart width="40vw" height="50vh" type="polarArea" [data]="data"></p-chart>
  `,
  styles: []
})
export class PolarAreaChartComponent implements OnInit {
  data: any;
  @Input() dataChart: Observable<any>;

  constructor() {}

  ngOnInit() {
    this.dataChart.subscribe(data => this.data = data);
  }
}
