import { NgModule } from '@angular/core';
import {ChartModule} from 'primeng/chart';

import { PolarAreaChartComponent } from './components/chart/polar-area-chart.component';

@NgModule({
  declarations: [PolarAreaChartComponent],
  imports: [
    ChartModule
  ],
  exports: [PolarAreaChartComponent]
})
export class PrimeFacesModule { }
